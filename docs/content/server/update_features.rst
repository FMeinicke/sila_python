Update features of a SiLA Server/Client package
===============================================

If you want to change features that you already added to the package, you can find the feature definitions (``.sila.xml`` files) in the ``generated`` subdirectory of the respective feature.
After you modified the feature definition, run :doc:`sila2-codegen update </content/code_generator/update>` from within the package direcory (use ``--package-dir`` to specify it from another location).

This will update the ``generated`` directory, and add ``updated_..._impl.py`` files to the ``feature_implementations`` directory.
These are empty feature implementations that you can use to migrate your implementation of the previous feature version to the new one.
The SiLA Server will continue to use the ``Impl`` class from ``..._impl.py``, so either adapt that existing file,
or move everything to ``updated_..._impl.py`` and then remove the ``updated_`` prefix from the filename to replace the old implementation.
