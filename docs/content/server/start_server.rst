Start the generated SiLA Server
===============================

The code snippets use the package name ``my_sila2_package``, as in the example :doc:`here <code_generator>`.
Use the name of your package instead.

Install the package
-------------------

After you :doc:`generated a SiLA Server/Client package <code_generator>`, you can install it from the directory where the file ``pyproject.toml`` is located:

- Use ``pip install -e .`` if you plan to modify the package files later. This installs the package in "editable" mode so that your changes are applied without requiring a re-installation. This will create a new directory ``my_sila2_package.egg-info``, which is required by the Python packaging infrastructure and can be ignored.
- Use ``pip install .`` instead, if you just want to install the package. This has to be run again if you make updates to the code. Two directories will be created: ``build`` and ``my_sila2_package.egg-info``. They can be ignored or deleted.

Run SiLA Server as module
-------------------------

The generated package contains a file ``__main__.py``, which makes it an executable Python module.
You can start the SiLA Server with ``python -m my_sila2_package [options]``.
Two options are required: ``--ip-address`` and ``--port``.
To run the server on your local device ("localhost") and port 50052, use ``python -m my_sila2_package --ip-address 127.0.0.1 --port 50052`` [encryption options].

There are multiple options regarding encrypted communication:

- Use ``--insecure`` to start the server using unencrypted communication.
    This violates the SiLA specification and should only be used for development purposes.
- Use ``--cert-file CERT_FILE`` and ``--private-key KEY_FILE`` to provide a PEM-encoded certificate and private key.
- Otherwise, the server will generate and use a self-signed certificate.
    This requires the library ``cryptography`` to be installed (``pip install cryptography`` or ``pip install sila2[cryptography]``).
    Use ``--ca-export-file CA_FILE`` to write the generated CA certificate to a file.
    Client applications might require this file to connect to the server.


Run ``python -m my_sila2_package --help`` to see all available options, e.g. on how to use your own certificates, or disable encrypted communication:

.. code-block::

    usage: python -m my_sila2_package [OPTIONS]

      Start this SiLA 2 server.

    Options:
      -a, --ip-address TEXT         The IP address  [default: 127.0.0.1]
      -p, --port INTEGER            The port  [default: 50052]
      --server-uuid TEXT            The server UUID [default: generate random
                                    UUID]
      --server-name TEXT            The server name [default: defined by
                                    implementation]
      --server-description TEXT     The server description [default: defined by
                                    implementation]
      --disable-discovery           Disable SiLA Server Discovery
      --insecure                    Start without encryption
      -k, --private-key-file TEXT   Private key file (e.g. 'server-key.pem')
      -c, --cert-file TEXT          Certificate file (e.g. 'server-cert.pem')
      --ca-file-for-discovery TEXT  Certificate Authority file for distribution
                                    via the SiLA Server Discovery (e.g. 'server-
                                    ca.pem')
      --ca-export-file TEXT         When using a self-signed certificate, write
                                    the generated CA to this file
      --quiet                       Only log errors
      --verbose                     Enable verbose logging
      --debug                       Enable debug logging
      --help                        Show this message and exit.

Run SiLA Server from Python
---------------------------

You can also run the server from within Python.
Just import the ``Server`` class from your package, instantiate it and use the :py:func:`~sila2.server.SilaServer.start` method, which has similar parameters as when executing the package:

.. code-block:: python

    from my_sila2_package import Server

    server = Server()

    try:
        server.start("127.0.0.1", 50052)
        # do something
    finally:
        server.stop()

For details on how to start the server, see the documentation of the :py:func:`~sila2.server.SilaServer.start` and :py:func:`~sila2.server.SilaServer.start_insecure` methods.
