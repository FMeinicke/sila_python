Class ObservableCommandInstance
===============================

.. autoclass:: sila2.server.ObservableCommandInstance

Class ObservableCommandInstanceWithIntermediateResponses
========================================================

.. autoclass:: sila2.server.ObservableCommandInstanceWithIntermediateResponses
