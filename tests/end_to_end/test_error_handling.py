from concurrent.futures import ThreadPoolExecutor

import grpc
import pytest
from grpc import RpcError

from sila2.client.sila_client import SilaClient
from sila2.features.silaservice import SiLAServiceFeature
from sila2.framework.abc.sila_error import SilaError
from sila2.framework.errors.command_execution_not_accepted import CommandExecutionNotAccepted
from sila2.framework.errors.defined_execution_error import DefinedExecutionError
from sila2.framework.errors.sila_connection_error import SilaConnectionError
from sila2.framework.errors.undefined_execution_error import UndefinedExecutionError
from sila2.framework.errors.validation_error import ValidationError
from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier
from sila2.framework.utils import raise_as_rpc_error
from sila2.server.sila_server import SilaServer
from tests.end_to_end.errorprovider_impl import ErrorProvider, ErrorProviderImpl
from tests.utils import generate_port


def test_server_side():
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.set_feature_implementation(ErrorProvider, ErrorProviderImpl(self))

    server = TestServer()
    port = generate_port()
    address = "127.0.0.1"

    server.start_insecure(address, port, enable_discovery=False)
    client = SilaClient(address, port, insecure=True)

    assert client.ErrorProvider.CheckIfEven(2).IsEven is True

    with pytest.raises(CommandExecutionNotAccepted):
        client.ErrorProvider.CheckIfEven(-2)
    with pytest.raises(ValidationError):
        client.ErrorProvider.CheckIfEven(-1)
    with pytest.raises(UndefinedExecutionError):
        client.ErrorProvider.CheckIfEven(0)
    with pytest.raises(DefinedExecutionError) as ex:
        client.ErrorProvider.CheckIfEven(1)
    assert (
        ex.value.fully_qualified_identifier
        == ErrorProvider.defined_execution_errors["Uneven"].fully_qualified_identifier
    )

    with pytest.raises(UndefinedExecutionError):
        client.ErrorProvider.UndefinedInvalidProperty.get()
    with pytest.raises(DefinedExecutionError):
        client.ErrorProvider.DefinedInvalidProperty.get()

    server.stop()

    with pytest.raises(SilaConnectionError):
        client.ErrorProvider.CheckIfEven(2)
    with pytest.raises(SilaConnectionError):
        client.ErrorProvider.DefinedInvalidProperty.get()
    assert not SilaError.is_sila_error(ValueError("Random error"))

    err = RpcError()
    err.code = lambda: grpc.StatusCode.UNIMPLEMENTED
    assert not SilaError.is_sila_error(err)

    err.code = lambda: grpc.StatusCode.ABORTED
    err.details = lambda: "123"
    assert not SilaError.is_sila_error(err)

    with pytest.raises(ValueError):
        SilaError.from_rpc_error(ValueError("Random error"), client)


def test_client_side():
    error_node = ErrorProvider.defined_execution_errors["TestError"]
    error = DefinedExecutionError(error_node, "error message")

    class SiLAServiceImpl(SiLAServiceFeature._servicer_cls):
        feature_definitions = {
            SiLAServiceFeature.fully_qualified_identifier: SiLAServiceFeature._feature_definition,
            ErrorProvider.fully_qualified_identifier: ErrorProvider._feature_definition,
        }

        def Get_ImplementedFeatures(self, request, context):
            return SiLAServiceFeature._unobservable_properties["ImplementedFeatures"].to_message(
                list(self.feature_definitions.keys())
            )

        def GetFeatureDefinition(self, request, context):
            feature_id = FullyQualifiedIdentifier(request.FeatureIdentifier.value)
            feature_definition = self.feature_definitions[feature_id]
            response = SiLAServiceFeature._unobservable_commands["GetFeatureDefinition"].responses.to_message(
                feature_definition
            )
            return response

    class ErrorProviderImpl(ErrorProvider._servicer_cls):
        def CheckIfEven(self, request, context):
            raise_as_rpc_error(error, context)

        def Get_UndefinedInvalidProperty(self, request, context):
            raise_as_rpc_error(error, context)

    server = grpc.server(ThreadPoolExecutor(max_workers=100))
    ErrorProvider._grpc_module.add_ErrorProviderServicer_to_server(ErrorProviderImpl(), server)
    SiLAServiceFeature._grpc_module.add_SiLAServiceServicer_to_server(SiLAServiceImpl(), server)
    port = generate_port()
    server.add_insecure_port(f"localhost:{port}")
    server.start()

    client = SilaClient("localhost", port, insecure=True)

    # check if client catches undefined DefinedExecutionErrors and converts them to UndefinedExecutionErrors
    with pytest.raises(UndefinedExecutionError):
        client.ErrorProvider.CheckIfEven(1)
    with pytest.raises(UndefinedExecutionError):
        client.ErrorProvider.UndefinedInvalidProperty.get()
