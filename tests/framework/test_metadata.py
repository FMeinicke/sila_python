import uuid
from typing import List, Tuple

import pytest

from sila2.client.sila_client import SilaClient
from sila2.framework.errors.defined_execution_error import DefinedExecutionError
from sila2.framework.errors.no_metadata_allowed import NoMetadataAllowed
from sila2.framework.feature import Feature
from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier
from sila2.framework.pb2 import SiLAFramework_pb2
from sila2.server import MetadataDict
from sila2.server.feature_implementation_base import FeatureImplementationBase
from sila2.server.sila_server import SilaServer
from tests.utils import create_server_stub, generate_port, get_feature_definition_str


@pytest.fixture
def metadata_feature() -> Feature:
    return Feature(get_feature_definition_str("Metadata"))


@pytest.fixture
def server_client(metadata_feature) -> Tuple[SilaServer, SilaClient]:
    meta = metadata_feature.metadata_definitions["Meta"]
    error_def = metadata_feature.defined_execution_errors["StringWasAbc"]

    class StringWasAbc(DefinedExecutionError):
        def __init__(self):
            super().__init__(error_def, error_def._description)

    class MetadataImpl(FeatureImplementationBase):
        def ReturnAllMetadata(self, metadata: MetadataDict) -> List[Tuple[str, str]]:
            if metadata is None:
                return []
            if metadata.get(meta.fully_qualified_identifier) == "Abc":
                raise StringWasAbc  # not in list of defined execution errors of this command
            return [(m.fully_qualified_identifier, str(value)) for m, value in metadata.items()]

        def ReturnMetadata(self, metadata: MetadataDict) -> str:
            value = metadata[meta.fully_qualified_identifier]
            if value == "Abc":
                raise StringWasAbc
            return value

        def get_calls_affected_by_IntMeta(self) -> List[FullyQualifiedIdentifier]:
            return [metadata_feature.fully_qualified_identifier]

        def get_calls_affected_by_Meta(self) -> List[FullyQualifiedIdentifier]:
            # allow metadata in SiLAService to allow NoMetadataAllowed
            return [
                metadata_feature.fully_qualified_identifier,
                FullyQualifiedIdentifier("org.silastandard/core/SiLAService/v1"),
            ]

        def get_calls_affected_by_BinaryMeta(self) -> List[FullyQualifiedIdentifier]:
            return [metadata_feature.fully_qualified_identifier]

    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_uuid=uuid.uuid4(),
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.set_feature_implementation(metadata_feature, MetadataImpl(self))

    server = TestServer()
    port = generate_port()
    address = "127.0.0.1"

    server.start_insecure(address, port, enable_discovery=False)
    client = SilaClient(address, port, insecure=True)

    return server, client


def test_node(metadata_feature):
    meta = metadata_feature.metadata_definitions["Meta"]

    assert meta._identifier == "Meta"
    assert meta._display_name == "Meta"
    assert meta._description == "A metadata definition"
    assert meta.fully_qualified_identifier == "de.unigoettingen/tests/Metadata/v1/Metadata/Meta"


def test_binary(metadata_feature):
    meta = metadata_feature.metadata_definitions["BinaryMeta"]
    _ = meta.to_message(b"abc" * 1024)

    with pytest.raises(ValueError):
        meta.to_message(b"abc" * 1024**2)

    binary_msg = SiLAFramework_pb2.Binary(binaryTransferUUID=str(uuid.uuid4()))
    meta_msg = metadata_feature._pb2_module.Metadata_BinaryMeta(BinaryMeta=binary_msg)
    with pytest.raises(ValueError):
        meta.to_native_type(meta_msg.SerializeToString())


def test_grpc(metadata_feature):
    meta = metadata_feature.metadata_definitions["Meta"]
    cmd = metadata_feature._unobservable_commands["ReturnMetadata"]

    class MetadataImpl(metadata_feature._servicer_cls):
        def ReturnMetadata(self, request, context):
            metadata = {m.key: m.value for m in context.invocation_metadata()}
            return cmd.responses.to_message(meta.to_native_type(metadata[meta.to_grpc_header_key()]))

        def Get_FCPAffectedByMetadata_Meta(self, request, context):
            return meta.to_affected_calls_message([cmd.fully_qualified_identifier])

    server, stub = create_server_stub(metadata_feature._grpc_module, MetadataImpl())

    response, call = stub.ReturnMetadata.with_call(
        cmd.parameters.to_message(),
        metadata=((meta.to_grpc_header_key(), meta.to_message("abcdef")),),
    )

    assert cmd.responses.to_native_type(response).Meta == "abcdef"


def test_affected_calls(server_client):
    server, client = server_client

    request = client._features["Metadata"]._pb2_module.Get_FCPAffectedByMetadata_Meta_Parameters()
    response = client._features["Metadata"]._grpc_stub.Get_FCPAffectedByMetadata_Meta(request)
    assert response.AffectedCalls[0].value == client._features["Metadata"].fully_qualified_identifier


def test_silaservice(server_client, metadata_feature):
    server, client = server_client
    metadata = [client.Metadata.Meta("teststring")]

    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.GetFeatureDefinition("org.silastandard/core/SiLAService/v1", metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.SetServerName("Abc", metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ServerName.get(metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ServerType.get(metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ServerUUID.get(metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ServerDescription.get(metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ServerVersion.get(metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ServerVendorURL.get(metadata=metadata)
    with pytest.raises(NoMetadataAllowed):
        client.SiLAService.ImplementedFeatures.get(metadata=metadata)
