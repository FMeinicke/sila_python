from sila2.framework.constraints.maximal_exclusive import MaximalExclusive
from sila2.framework.data_types.date import Date
from sila2.framework.data_types.integer import Integer
from sila2.framework.data_types.real import Real


def test(silaframework_pb2_module):
    m_int = MaximalExclusive("3", base_type=Integer())
    assert m_int.validate(2)
    assert not m_int.validate(3)
    assert not m_int.validate(3)

    assert repr(m_int) == "MaximalExclusive(3.0)"

    m_float = MaximalExclusive("2.5", base_type=Real())
    assert m_float.validate(2.4)
    assert not m_float.validate(2.5)
    assert not m_float.validate(3)

    m_date = MaximalExclusive("2021-01-01Z", base_type=Date())
    assert repr(m_date) == "MaximalExclusive('2021-01-01Z')"


def test_scientific_notation(silaframework_pb2_module):
    m_int = MaximalExclusive("3e5", base_type=Integer())
    m_float = MaximalExclusive("1.23e-4", base_type=Real())

    assert not m_int.validate(300_000)
    assert m_int.validate(299_999)

    assert not m_float.validate(0.000123)
    assert m_float.validate(0.0001229)
