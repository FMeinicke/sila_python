import pytest

from sila2.framework.data_types.integer import Integer
from sila2.framework.pb2 import SiLAFramework_pb2


def test_to_native_type():
    val = Integer().to_native_type(SiLAFramework_pb2.Integer(value=10))
    assert isinstance(val, int)
    assert val == 10


def test_to_message():
    msg = Integer().to_message(10)
    assert msg.value == 10


def test_wrong_type():
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        Integer().to_message(1.0)


def test_out_of_range():
    integer_field = Integer()

    integer_field.to_message(2**63 - 1)
    with pytest.raises(ValueError):
        integer_field.to_message(2**63 + 1)

    integer_field.to_message(-1 * 2**63)
    with pytest.raises(ValueError):
        integer_field.to_message(-1 * 2**63 - 1)


def test_from_string():
    integer_field = Integer()

    assert integer_field.from_string("1") == 1
    with pytest.raises(ValueError):
        _ = integer_field.from_string("1.")
