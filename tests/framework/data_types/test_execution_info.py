from datetime import timedelta
from math import isclose

import pytest

from sila2.framework.command.execution_info import CommandExecutionInfo, CommandExecutionStatus, ExecutionInfo


@pytest.fixture
def execution_info_field(silaframework_pb2_module) -> ExecutionInfo:
    return ExecutionInfo(silaframework_pb2_module)


def test_all_set(execution_info_field, silaframework_pb2_module):
    status = CommandExecutionInfo(CommandExecutionStatus.running, 0.4, timedelta(seconds=3), timedelta(seconds=2))
    msg = execution_info_field.to_message(status)
    res = execution_info_field.to_native_type(msg)

    assert res.status == CommandExecutionStatus.running
    assert isclose(res.progress, 0.4)
    assert res.estimated_remaining_time == timedelta(seconds=3)
    assert res.updated_lifetime_of_execution == timedelta(seconds=2)

    assert isinstance(msg, silaframework_pb2_module.ExecutionInfo)


def test_no_optional_values(execution_info_field):
    status = CommandExecutionInfo(CommandExecutionStatus.finishedSuccessfully, None, None, None)
    msg = execution_info_field.to_message(status)
    res = execution_info_field.to_native_type(msg)

    assert res.status == CommandExecutionStatus.finishedSuccessfully
    assert res.progress is None
    assert res.estimated_remaining_time is None
    assert res.updated_lifetime_of_execution is None
